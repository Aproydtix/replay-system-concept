﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using GamepadInput;

public class InputManager : MonoBehaviour
{
    MainScript ms;

    //Input
    public float deadZone = .1f;    //  Axis deadzone on the Gamepad
    public bool a;      //  A button, Cross on PS
    public bool b;      //  B button, Circle, on PS
    public bool x;      //  X button, Square on PS
    public bool y;      //  Y button, Triangle on PS
    public bool start;  //  Start button
    public bool select; //  Select button
    public bool l1;     //  Left shoulder button
    public bool r1;     //  Right shoulder button
    public float l2;    //  Left shoulder trigger
    public float r2;    //  Right shoulder trigger
    public Vector2 ls;  //  Left Stick, x and y axis
    public Vector2 rs;  //  Right Stick, x and y axis
    public Vector2 dp;  //  D-pad, x and y axis

    //  Max 3 bits for each to save to 32-bit, max 7 for 64-bit
    int axisBit = 7;    //  How many bits each axis will be converted to
    int dpadBit = 7;    //  How many bits each dpad axis will be converted to
    int triggerBit = 7; //  How many bits each trigger will be converted to
    int ab;             //  axisBit converted to dec
    int db;             //  dpadBit converted to dec
    int tb;             //  triggerBit converted to dec

    // Use this for initialization
    void Start()
    {
        ms = GetComponent<MainScript>();

        string sab = ""; for (int i = 0; i < axisBit; i++) sab += "1";
        string sdb = ""; for (int i = 0; i < dpadBit; i++) sdb += "1";
        string stb = ""; for (int i = 0; i < triggerBit; i++) stb += "1";
        ab = System.Convert.ToInt32(sab, 2);
        db = System.Convert.ToInt32(sdb, 2);
        tb = System.Convert.ToInt32(stb, 2);
    }

    // Update is called once per frame
    void FixedUpdate()
    {

    }

    public void PlayerInput()
    {
        a = (Input.GetKey(KeyCode.Space) || GamePad.GetButton(GamePad.Button.A, GamePad.Index.Any));
        b = (Input.GetKey(KeyCode.X) || GamePad.GetButton(GamePad.Button.B, GamePad.Index.Any));
        x = (Input.GetKey(KeyCode.Z) || GamePad.GetButton(GamePad.Button.X, GamePad.Index.Any));
        y = (Input.GetKey(KeyCode.C) || GamePad.GetButton(GamePad.Button.Y, GamePad.Index.Any));
        ls = new Vector2(
            ((Input.GetKey(KeyCode.D)) ? 1 : 0) - ((Input.GetKey(KeyCode.A)) ? 1 : 0),
            ((Input.GetKey(KeyCode.W)) ? 1 : 0) - ((Input.GetKey(KeyCode.S)) ? 1 : 0))
            +
            GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Any);
        rs = new Vector2(
            ((Input.GetKey(KeyCode.RightArrow)) ? 1 : 0) - ((Input.GetKey(KeyCode.LeftArrow)) ? 1 : 0),
            ((Input.GetKey(KeyCode.UpArrow)) ? 1 : 0) - ((Input.GetKey(KeyCode.DownArrow)) ? 1 : 0))
            +
            GamePad.GetAxis(GamePad.Axis.RightStick, GamePad.Index.Any);
        dp = new Vector2(
            ((Input.GetKey(KeyCode.H)) ? 1 : 0) - ((Input.GetKey(KeyCode.F)) ? 1 : 0),
            ((Input.GetKey(KeyCode.T)) ? 1 : 0) - ((Input.GetKey(KeyCode.G)) ? 1 : 0))
            +
            GamePad.GetAxis(GamePad.Axis.Dpad, GamePad.Index.Any);
        start = (Input.GetKey(KeyCode.Escape) || GamePad.GetButton(GamePad.Button.Start, GamePad.Index.Any));
        select = (Input.GetKey(KeyCode.Backspace) || GamePad.GetButton(GamePad.Button.Back, GamePad.Index.Any));
        l1 = (Input.GetKey(KeyCode.V) || GamePad.GetButton(GamePad.Button.LeftShoulder, GamePad.Index.Any));
        r1 = (Input.GetKey(KeyCode.B) || GamePad.GetButton(GamePad.Button.RightShoulder, GamePad.Index.Any));
        l2 = ((Input.GetKey(KeyCode.N)) ? 1 : 0) + GamePad.GetTrigger(GamePad.Trigger.LeftTrigger, GamePad.Index.Any);
        r2 = ((Input.GetKey(KeyCode.M)) ? 1 : 0) + GamePad.GetTrigger(GamePad.Trigger.RightTrigger, GamePad.Index.Any);


        //  Clamping all axis values
        ls = (ls.magnitude > deadZone) ? new Vector2(Mathf.Clamp(ls.x, -1f, 1f), Mathf.Clamp(ls.y, -1f, 1f)) : Vector2.zero;
        rs = (rs.magnitude > deadZone) ? new Vector2(Mathf.Clamp(rs.x, -1f, 1f), Mathf.Clamp(rs.y, -1f, 1f)) : Vector2.zero;
        dp = (dp.magnitude > deadZone) ? new Vector2(Mathf.Clamp(dp.x, -1f, 1f), Mathf.Clamp(dp.y, -1f, 1f)) : Vector2.zero;
        l2 = (l2 > deadZone) ? Mathf.Clamp(l2, 0, 1f) : 0;
        r2 = (r2 > deadZone) ? Mathf.Clamp(r2, 0, 1f) : 0;


        //  All floats are set to have a maximum of X decimals(binary)
        float l2i = Mathf.RoundToInt(l2 * tb) / (float)tb;
        float r2i = Mathf.RoundToInt(r2 * tb) / (float)tb;

        int axis = ab / 2;
        float lsx = Mathf.RoundToInt(ls.x * axis) / (float)axis;
        float lsy = Mathf.RoundToInt(ls.y * axis) / (float)axis;
        float rsx = Mathf.RoundToInt(rs.x * axis) / (float)axis;
        float rsy = Mathf.RoundToInt(rs.y * axis) / (float)axis;

        int dpad = db / 2;
        float dpx = Mathf.RoundToInt(dp.x * dpad) / (float)dpad;
        float dpy = Mathf.RoundToInt(dp.y * dpad) / (float)dpad;


        ls = new Vector2(lsx, lsy);
        rs = new Vector2(rsx, rsy);
        dp = new Vector2(dpx, dpy);
        l2 = l2i;
        r2 = r2i;
    }

    public void CompressAndSaveInput()
    {
        //  Converting all bools to ints
        long ai = ((a) ? 1 : 0);
        long bi = ((b) ? 1 : 0);
        long xi = ((x) ? 1 : 0);
        long yi = ((y) ? 1 : 0);
        long starti = ((start) ? 1 : 0);
        long selecti = ((select) ? 1 : 0);
        long l1i = ((l1) ? 1 : 0);
        long r1i = ((r1) ? 1 : 0);

        //  Converting axis to ints
        long l2i = (Mathf.RoundToInt(l2 * tb));
        long r2i = (Mathf.RoundToInt(r2 * tb));

        int axis = ab / 2;
        long lsx = (Mathf.RoundToInt(ls.x * axis + axis + 1));
        long lsy = (Mathf.RoundToInt(ls.y * axis + axis + 1));
        long rsx = (Mathf.RoundToInt(rs.x * axis + axis + 1));
        long rsy = (Mathf.RoundToInt(rs.y * axis + axis + 1));

        int dpad = db / 2;
        long dpx = (Mathf.RoundToInt(dp.x * dpad + dpad + 1));
        long dpy = (Mathf.RoundToInt(dp.y * dpad + dpad + 1));

        //  Bitshifting into a single variable
        long input = 0;
        //Buttons
        input = input | (ai << 0);        
        input = input | (bi << 1);        
        input = input | (xi << 2);       
        input = input | (yi << 3);  
        input = input | (starti << 4);   
        input = input | (selecti << 5);   
        input = input | (l1i << 6);       
        input = input | (r1i << 7);       
        //Unsigned axis
        input = input | (l2i << 8);       
        input = input | (r2i << (8+triggerBit));      
        //Signed axis
        input = input | (lsx << (8+triggerBit*2));      
        input = input | (lsy << (8+triggerBit*2+axisBit));     
        input = input | (rsx << (8+triggerBit*2+axisBit*2));      
        input = input | (rsy << (8+triggerBit*2+axisBit*3));      

        input = input | (dpx << (8+triggerBit*2+axisBit*4));      
        input = input | (dpy << (8+triggerBit*2+axisBit*4+dpadBit));      


        //replay.Add(input);
        //Debug.Log(System.Convert.ToString(input, 2));

        ms.bwrite.Write(input);                         //  Write input to file
    }

    public void LoadAndUncompressInput()
    {
        long input;
        try
        {
            input = ms.bread.ReadInt64();  //  Input from file
        }
        catch (IOException e)   //  Read the next line to input
        {                       //      if it couldn't read
            Debug.Log("End of file!\n" + e.Message);    //  Debug
            ms.closeFile = true;                        //  Close the file being read
            return;                                     //  Return
        }


        //replay.Add(input);                              
        //Debug.Log(System.Convert.ToString(input, 2));

        //  Getting values from the saved input
        int axis = ab / 2;
        int dpad = db / 2;

        //Buttons
        a = (((input >> 0) & 1) == 1);          
        b = (((input >> 1) & 1) == 1);          
        x = (((input >> 2) & 1) == 1);           
        y = (((input >> 3) & 1) == 1);             
        start = (((input >> 4) & 1) == 1);          
        select = (((input >> 5) & 1) == 1);         
        l1 = (((input >> 6) & 1) == 1);                
        r1 = (((input >> 7) & 1) == 1);                
        //Unsigned axis
        l2 = ((input >> 8) & tb) / (float)tb;              
        r2 = ((input >> (8+triggerBit)) & tb) / (float)tb;      
        //Signed axis
        float lsx = ((int)((input >> (8+triggerBit*2)) & ab)-1-axis) / (float)axis;  
        float lsy = ((int)((input >> (8+triggerBit*2+axisBit)) & ab)-1-axis) / (float)axis;  
        float rsx = ((int)((input >> (8+triggerBit*2+axisBit*2)) & ab)-1-axis) / (float)axis;  
        float rsy = ((int)((input >> (8+triggerBit*2+axisBit*3)) & ab)-1-axis) / (float)axis;  

        float dpx = ((int)((input >> (8+triggerBit*2+axisBit*4)) & db)-1-dpad) / (float)dpad;  
        float dpy = ((int)((input >> (8+triggerBit*2+axisBit*4+dpadBit)) & db)-1-dpad) / (float)dpad;  

        ls = new Vector2(lsx, lsy);     //  Set Left Stick
        rs = new Vector2(rsx, rsy);     //  Set Right Stick
        dp = new Vector2(dpx, dpy);     //  Set D-pad

        //  Clamping all axis values
        ls = (ls.magnitude > deadZone) ? new Vector2(Mathf.Clamp(ls.x, -1f, 1f), Mathf.Clamp(ls.y, -1f, 1f)) : Vector2.zero;
        rs = (rs.magnitude > deadZone) ? new Vector2(Mathf.Clamp(rs.x, -1f, 1f), Mathf.Clamp(rs.y, -1f, 1f)) : Vector2.zero;
        dp = (dp.magnitude > deadZone) ? new Vector2(Mathf.Clamp(dp.x, -1f, 1f), Mathf.Clamp(dp.y, -1f, 1f)) : Vector2.zero;
        l2 = (l2 > deadZone) ? Mathf.Clamp(l2, 0, 1f) : 0;
        r2 = (r2 > deadZone) ? Mathf.Clamp(r2, 0, 1f) : 0;
    }
}
