﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ice")
        {
            GetComponent<BoxCollider2D>().isTrigger = false;
            GetComponent<SpriteRenderer>().color = Color.cyan;
        }
    }
}
