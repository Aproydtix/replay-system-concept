﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour
{
    public int hp = 100;
    public GameObject target;
    bool targetFound = false;
    public Element element = Element.Darkness;
    public GameObject eye;
    public GameObject spellPrefab;
    float spellCD;
    float maxSpeed = 2;
    public bool hit;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (hp <= 0)
            Destroy(gameObject);

        if (!targetFound)
        {
            if (Vector3.Distance(target.transform.position, transform.position) < 5)
            {
                element = target.GetComponent<PlayerScript>().element;
                targetFound = true;
            }
            return;
        }

        if (element == Element.Fire)
            eye.GetComponent<SpriteRenderer>().color = Color.red;
        if (element == Element.Ice)
            eye.GetComponent<SpriteRenderer>().color = Color.cyan;
        if (element == Element.Lightning)
            eye.GetComponent<SpriteRenderer>().color = Color.yellow;
        if (element == Element.Water)
            eye.GetComponent<SpriteRenderer>().color = Color.blue;

        Vector3 vec = transform.position - target.transform.position;
        eye.transform.RotateAround(
            transform.position,
            transform.forward,
            -eye.transform.eulerAngles.z + ((vec.y >= 0) ? Vector2.Angle(Vector2.right, vec) : -Vector2.Angle(Vector2.right, vec))
            );


        if (Vector3.Distance(target.transform.position, transform.position) > 2)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, maxSpeed * Time.fixedDeltaTime);
        }

        if (spellCD <= 0)
        {
            GameObject spell = Instantiate(spellPrefab, eye.transform.position, eye.transform.rotation);
            spell.GetComponent<SpriteRenderer>().color = Color.black;
            spell.GetComponent<SpellScript>().element = Element.Darkness;
            spell.GetComponent<SpellScript>().duration = 5f;
            spell.GetComponent<Rigidbody2D>().velocity = -new Vector2(Mathf.Cos(eye.transform.eulerAngles.z * Mathf.Deg2Rad), Mathf.Sin(eye.transform.eulerAngles.z * Mathf.Deg2Rad)) * maxSpeed;
            spellCD = .25f;

        }
        else
            spellCD -= Time.fixedDeltaTime;

        if (hit)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
            hit = false;
        }
        else
            GetComponent<SpriteRenderer>().color = Color.white;
    }

    
}
