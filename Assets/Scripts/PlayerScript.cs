﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    public GameObject main;
    InputManager im;

    //Stats
    public float acceleration = 1f;
    public float speed = 0f;
    public float maxSpeed = 5f;
    public float jumpForce = 50f;
    bool isGrounded = false;
    bool prevGrounded = false;
    bool doublejump = false;
    public Element element = Element.Fire;

    //Input
    bool a;
    bool prevA;

    //Components
    Rigidbody2D rb;
    GameObject arm;
    public GameObject spellPrefab;
    float spellCD;
    public int hp = 10;
    int startHp;
    Vector3 spawnPoint;
    bool hit = false;

	// Use this for initialization
	void Start ()
    {
        spawnPoint = transform.position;
        startHp = hp;
        im = main.GetComponent<InputManager>();
        rb = GetComponent<Rigidbody2D>();
        arm = transform.Find("Arm").gameObject;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        Movement();
        Elements();

        if (hit)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
            hit = false;
        }
        else
            GetComponent<SpriteRenderer>().color = Color.white;
        if (hp <= 0)
            Respawn();
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.transform.tag == "Ground")
        {
            isGrounded = true;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "Ground")
        {
            isGrounded = true;
        }
    }

    void Movement()
    {
        a = im.a;

        if (prevGrounded && rb.velocity.y == 0)
        {
            rb.velocity += new Vector2(im.ls.x * acceleration, 0);
            if (im.ls.x == 0)
                rb.velocity = new Vector2(rb.velocity.x / 1.2f, rb.velocity.y);

            if (a && !prevA)
                rb.velocity += new Vector2(0, jumpForce);

            doublejump = true;
        }
        else
        {
            rb.velocity += new Vector2(im.ls.x * (acceleration / 2f), 0);
            if (im.ls.x == 0)
                rb.velocity = new Vector2(rb.velocity.x / (1.2f) / 2f, rb.velocity.y);

            if (doublejump && a && !prevA)
            {
                doublejump = false;
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y / 4f);
                rb.velocity += new Vector2(0, jumpForce);
            }
        }

        if (Mathf.Abs(rb.velocity.x) > maxSpeed)
            rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);

        prevGrounded = isGrounded;
        prevA = im.a;
    }

    void Elements()
    {
        if (im.dp.y > .5f)
            element = Element.Fire;
        if (im.dp.y < -.5f)
            element = Element.Ice;
        if (im.dp.x > .5f)
            element = Element.Lightning;
        if (im.dp.x < -.5f)
            element = Element.Water;

        if (element == Element.Fire)
            arm.GetComponent<SpriteRenderer>().color = Color.red;
        if (element == Element.Ice)
            arm.GetComponent<SpriteRenderer>().color = Color.cyan;
        if (element == Element.Lightning)
            arm.GetComponent<SpriteRenderer>().color = Color.yellow;
        if (element == Element.Water)
            arm.GetComponent<SpriteRenderer>().color = Color.blue;

        Debug.DrawLine(transform.position, transform.position + (Vector3)im.rs);
        arm.transform.RotateAround(
            transform.position,
            transform.forward,
            -arm.transform.eulerAngles.z + ((im.rs.y >= 0)? Vector2.Angle(Vector2.right, im.rs) : -Vector2.Angle(Vector2.right, im.rs))
            );

        if (spellCD <= 0)
        {
            if (im.r1)
            {
                GameObject spell = Instantiate(spellPrefab, arm.transform.position, arm.transform.rotation);
                spell.GetComponent<SpriteRenderer>().color = arm.GetComponent<SpriteRenderer>().color;
                spell.GetComponent<SpellScript>().element = element;
                spell.GetComponent<SpellScript>().duration = 5f;
                spell.GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Cos(arm.transform.eulerAngles.z*Mathf.Deg2Rad), Mathf.Sin(arm.transform.eulerAngles.z*Mathf.Deg2Rad)) * maxSpeed;
                spellCD = .25f;
            }
        }
        else
            spellCD -= Time.fixedDeltaTime;
    }

    void Respawn()
    {
        hp = startHp;
        transform.position = spawnPoint;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Respawn")
        {
            hp = startHp;
            spawnPoint = collision.transform.position;
        }

        if (collision.tag == "Darkness")
        {
            hp--;
            hit = true;
            Destroy(collision.gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Death")
            Respawn();
    }
}
