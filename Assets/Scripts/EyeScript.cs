﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeScript : MonoBehaviour {

    BossScript bs;

	// Use this for initialization
	void Start () {
        bs = transform.parent.GetComponent<BossScript>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Fire" && bs.element == Element.Fire
            || collision.tag == "Ice" && bs.element == Element.Ice
            || collision.tag == "Lightning" && bs.element == Element.Lightning
            || collision.tag == "Water" && bs.element == Element.Water)
        {
            bs.hit = true;
            bs.hp--;
            Destroy(collision.gameObject);
        }
    }
}
