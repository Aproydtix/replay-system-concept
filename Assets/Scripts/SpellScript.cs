﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Element { Fire, Ice, Lightning, Water, Darkness };

public class SpellScript : MonoBehaviour {

    public float duration;
    public Element element;

    // Use this for initialization
    void Start ()
    {
        if (element == Element.Fire)
            transform.tag = "Fire";
        if (element == Element.Ice)
            transform.tag = "Ice";
        if (element == Element.Lightning)
            transform.tag = "Lightning";
        if (element == Element.Water)
            transform.tag = "Water";
        if (element == Element.Darkness)
            transform.tag = "Darkness";
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        duration -= Time.fixedDeltaTime;
        if (duration <= 0)
            Destroy(gameObject);
	}
}
