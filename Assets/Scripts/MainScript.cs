﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using GamepadInput;

public class MainScript : MonoBehaviour {

    InputManager im;
    
    //Files
    public BinaryWriter bwrite;     //  Writes to file
    public BinaryReader bread;      //  Reads from file
    bool writeToFile = false;       //  Write to file
    bool readFromFile = false;      //  Read from file
    public bool closeFile = false;  //  Close the current file
    FileInfo[] files;               //  Files available to read
    int fileRead = -1;              //  The file read for replay
    string user = "User";           //  Username
    float timeLimit = 60*60*60;     //  Closes the file when reaching the time limit (Uses each input save as reference)

    //Game
    bool gui = true;
    bool pause = true;

    // Use this for initialization
    void Start ()
    {
        im = GetComponent<InputManager>();
    }

    void OnGUI()
    {
        float w = Screen.width / 1920f;     //ratio to keep scaling regardless of resolution
        float h = Screen.height / 1080f;    //ratio to keep scaling regardless of resolution
        float bw = 150;     //Button width
        float bh = 60;      //Button height
        float bs = 10;      //Button space
        int bc = 0;         //Button count
        GUI.skin.button.fontSize = (int)(h * 40f * (w / h));    //Set fontsize

        //Show GUI
        bc++;
        GUI.color = (gui) ? Color.white : Color.grey;
        if (GUI.Button(new Rect(w * bs, h * bs * bc + h * bh * (bc - 1), w * bw, h * bh), "GUI"))
            gui = !gui;

        if (!gui)
            return;


        //Reset Button
        bc++;
        GUI.color = Color.red;         //Red
        if (GUI.Button(new Rect(w * bs + w * bw / 2f, h * bs * bc + h * bh * (bc - .5f), w * bw, h * bh), "Reset"))
        {
            if (readFromFile)
                bread.Close();
            if (writeToFile)
                bwrite.Close();
            Application.LoadLevel(Application.loadedLevel); //Reload current level
        }
        bc++;


        //writeToFile Button
        bc++;
        GUI.color = Color.grey;         //Grey
        if (writeToFile)
            GUI.color = Color.white;    //White if active
        //If Button clicked
        GUI.Button(new Rect(w * bs, h * bs * bc + h * bh * (bc - 1), w * bw, h * bh), "Write:");
        if (GUI.Button(new Rect(w * bs + w * bw, h * bs * bc + h * bh * (bc - 1), w * bw, h * bh), (writeToFile) ? "On" : "Off"))
        {
            writeToFile = !writeToFile;

            if (writeToFile)
            {
                string dt = string.Format("{0:yyyy-MM-dd-hh-mm}", System.DateTime.Now);
                bwrite = new BinaryWriter(new FileStream("Replay-" + user + "-" + dt + ".mvg", FileMode.Create));

                readFromFile = false;
                closeFile = false;
                pause = false;
            }
            else
            {
                bwrite.Close();
                pause = true;
            }
        }

        //readFromFile Button
        bc++;
        GUI.color = Color.grey;         //Grey
        if (readFromFile)
            GUI.color = Color.white;    //White if active
        //If Button clicked
        GUI.Button(new Rect(w * bs, h * bs * bc + h * bh * (bc - 1), w * bw, h * bh), "Read:");
        if (GUI.Button(new Rect(w * bs + w * bw, h * bs * bc + h * bh * (bc - 1), w * bw, h * bh), (readFromFile) ? "On":"Off"))
        {
            readFromFile = !readFromFile;

            if (readFromFile)
            {
                DirectoryInfo di = new DirectoryInfo(Directory.GetCurrentDirectory());
                files = di.GetFiles("Replay-" + "*.mvg");

                writeToFile = false;
            }
            else
                pause = true;
        }

        if (readFromFile)
            for (int i = 0; i < files.GetLength(0); i++)
            {
                //File Buttons
                bc++;
                GUI.color = Color.white;         //White
                if (fileRead == i)
                    GUI.color = Color.green;         //Green
                //If Button clicked
                GUI.Button(new Rect(w * bs, h * bs * bc + h * bh * (bc - 1), w * bw, h * bh), "File " + (i + 1).ToString() + ":");
                if (GUI.Button(new Rect(w * bs + w * bw, h * bs * bc + h * bh * (bc - 1), w * bw * files[i].Name.Length/6, h * bh), files[i].Name) && pause)
                {
                    bread = new BinaryReader(new FileStream(files[i].Name, FileMode.Open));
                    fileRead = i;
                    closeFile = false;
                    pause = false;
                }
            }
        
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        if (pause)
            return;

        //Take input from player or file
        if (!readFromFile)
            im.PlayerInput();
        else
        {
            im.LoadAndUncompressInput();

            if (closeFile)
            {
                bread.Close();
                readFromFile = false;
                pause = true;
            }
        }

        //Game
        MoveCamera();

        //Save Input to file
        if (writeToFile)
        {
            im.CompressAndSaveInput();
            timeLimit--;

            if (timeLimit <= 0)
                closeFile = true;

            if (closeFile)
            {
                bwrite.Close();
                writeToFile = false;
                pause = true;
            }
        }
	}

    void MoveCamera()
    {
        transform.position = GameObject.Find("Player").transform.position;                  //  Move to Player
        transform.position = new Vector3(transform.position.x, transform.position.y, -10);  //  Move Camera back
    }
}
