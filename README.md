# Replay System Concept
Made in Unity using C#.

Replay system built around recording and compressing input data from a gamepad. Every frame it records every button and axis on one controller and compresses it down to 64 bits. This data can then be further compressed with compression algorithms, and I did so using 7zip. 1 hour of recorded inputs ended up taking 1,688 KB before compression, and 17 KB after.

It is important that the gameplay itself is deterministic for this to function, as such the physics update loop is used for game logic. To implement randomness in the game, a set random seed, or the outcome of each random action, has to be created and stored with the input data so that the randomness is consistent in the replay data.

The gameplay itself is extremely crude and hastily implemented.

## Build
A build can be found as a .zip file in the Downloads section.

## Video Demo
https://youtu.be/CA08AXrAqzU

## Screenshots
![Screenshot1](Screenshot1.png)
